# Arrahook

A server that executes arbitrary commands based on the content of a HTTP request.
Indended for use with Git Web Hooks.

## Contents
1. [Getting Started](#getting-started)
2. [Requirements](#requirements)
3. [Basic Structure](#basic-structure)
4. [Configuration](#configuration)
    1. [config.json](#configjson)
    2. [Hooks](#hooks-1)
5. [Security Concerns](#security-concerns)

----

## Getting Started
1. Pull repo to location of choice.
2. Configure.
3. Run.

## Requirements
- There must be a configuration file named `config.json` in this (project root) directory. For your ease-of-use, `config.json` has been added to this project's `.gitignore`.

## Basic Structure
1. A Web Hook sends a request to this server (Arrahook).
2. Arrahook searches for matching hooks.
3. The matched hooks are sorted by priority.
4. The hooks have their actions executed, in order, until either there are no hooks left, or a hook was configured to end the request early.

## Configuration

### config.json

See `example.config.json` for sensible defaults.

#### port
Type: `Number`
Required: `Yes`

The port to listen on.

#### maximumRequestLength
Type: `Number`
Required: `Yes`

The maximum incoming request length, in bytes.

#### hooks
Type: `String`
Required: `Yes`

A Node `require` path to where hooks can be loaded from. Can be JSON or a module. Expects to load an array of hooks.

For example, you could create a module that exports an array of your project's hooks. This can help with organising large numbers of hooks at once in a structured way. Hooks are not reloaded at any point.

#### logActionlessRequests
Type: `Boolean`
Required: `No`

A toggle for logging requests that don't match any configured hooks. This can be useful for debugging or setting up new hooks.

#### processedHistoryCache
Type: `String`
Required: `No`

A file that will be read/written from/to. Serves as a way to track which hooks have already been completed and which haven't.

----

### Hooks

Arrahook expects an array of these. The structure of a hook is outlined below:

#### hook.name
Type: `String`
Required: `Yes`

Any string. Not used as an identifier, only as a way to improve logging.

#### hook.group
Type: `String`
Required: `No`

Any string. Used to group `hook.id`s together for history checking.

#### hook.id
Type: `String`
Required: `No`

A line of JavaScript to get the current hook's id. Used with `hook.group` to track if the hook has already been processed. Useful if your Web Hook provider sometimes sends more than one request.

For example: The incoming request contains a unique identifier, in the request body: `id`. The appropriate value for `hook.id` would be: `'request.body.id'`.

#### hook.stopHere
Type: `Boolean`
Required: `No`

If this value is true, and this hook is matched, do not match any more hooks.

#### hook.detailedLogging
Type: `Boolean`
Required: `No`

If this value is true, full request details will be logged once this hook is matched. Any actions will also be logged as they happen. Useful for complicated hooks or debugging.

#### hook.priority
Type: `Number`
Required: `No`

A value representing the hook's priority. Defaulting to 0, set this value lower to process the hook earlier.

#### hook.filters
Type: `Array.String`
Required: `Yes`

An array of strings containing `eval`able JavaScript. If any of the returned values are truthy, the hook will be processed for that request.

#### hook.actions
A mixed array of strings and objects.

When a string is provided, it is converted into an object using the template: `{ command: STRING, stopOnError: true }`

##### action.command
Type: `String`
Required: `Yes`

The command to run, with space-separated arguments.

##### action.stopOnError
Type: `Boolean`
Required: `No`

If this value is true, and the command returns an error, the current request will be aborted.

##### action.waitForExit
Type: `Boolean`
Required: `No`

Defaulting to `true`, when false, the next action will execute immediately. If this action fails, nothing happens, though it will be logged.

## Security Concerns

No authentication or verification is built into this server. It is entirely possible for a malicious user to trigger a hook using a hand-crafted request. You can mitigate against this by configuring a SSL-enabled reverse proxy, and setting up your webhook provider to verify SSL certificates.

Currently, you'll also need to add a filter rule to each hook:
```javascript
    request.socket.remoteAddress === '127.0.0.1'
```
However support for only allowing internal traffic (or traffic from specific IPs) is on the to-do list.
