/* jslint node: true, continue: true, evil: true */
'use strict';

var http = require('http');
var config = require('./config');
var hooks = require(config.hooks);
var Promise = require('promise');
var exec = Promise.denodeify(require('child_process').exec);
var readFile = Promise.denodeify(require('fs').readFile);
var writeFile = Promise.denodeify(require('fs').writeFile);
var statFile = Promise.denodeify(require('fs').stat);
var Moniker = require('moniker');
var identifierNamer = Moniker.generator([Moniker.verb, Moniker.adjective, Moniker.noun]);
require('console-stamp')(console, 'HH:MM:ss dd/mm/yy');

var processedHistory = {};

function respondFinished(response, code, message) {
    response.writeHead(code, { 'Content-Length': message.length, 'Content-Type': 'text/plain' });
    response.end(message);
}

function hookRequest(request, response) {
    var body = '';
    request.on('data', function (data) {
        body += data;
        if (body.length > config.maximumRequestLength) {
            request.connection.destroy();
        }
    });
    request.on('end', function () {
        var i, o, filter, hook,
            matchedHooks = [],
            skippedHooks = [],
            passedFilters,
            matchPromise, matchActionCount, matchedHook;

        respondFinished(response, 200, 'OK');

        request.identifier = identifierNamer.choose();

        try {
            request.body = JSON.parse(body);
        } catch (error) {
            if (typeof body !== 'string') {
                console.error('Received no data for %s. Method used: %s', request.identifier, request.method);
            } else {
                console.error('Data received for %s was invalid JSON. Method used: %s\r\nBody: %s', request.identifier, request.method, body);
            }

            return;
        }

        for (i = 0; i < hooks.length; i += 1) {
            hook = hooks[i];

            passedFilters = 0;
            for (o = 0; o < hook.filters.length; o += 1) {
                filter = hook.filters[o];

                if (eval(filter)) {
                    passedFilters += 1;
                }
            }

            if (passedFilters === hook.filters.length) {
                if (hook.group && hook.id && processedHistory[hook.group]) {
                    if (processedHistory[hook.group].indexOf(eval(hook.id)) !== -1) {
                        console.warn('Skipping hook for %s: %s', request.identifier, hook.name);
                        skippedHooks.push(hook);

                        continue;
                    }
                }

                matchedHooks.push(hook);
            }
        }

        matchedHooks.sort(function (a, b) { return (a.priority || 0) - (b.priority || 0); });

        // Determine if we need to log the request details.
        if (matchedHooks.reduce(function (prev, hook) { return prev || hook.detailedLogging; }, false)) {
            console.info('Logging request contents of %s:\r\nHeaders: %j\r\nBody: %j',
                request.identifier,
                request.headers,
                request.body);
        }

        matchPromise = Promise.resolve();
        matchActionCount = 0;
        for (i = 0; i < matchedHooks.length; i += 1) {
            matchedHook = matchedHooks[i];

            console.info('Queueing hook for %s: %s', request.identifier, matchedHook.name);

            matchPromise = matchedHook.actions.reduce(function (promise, action, index) {
                matchActionCount += 1;
                return promise.then(function () {
                    var command = typeof action === 'string' ? action : action.command,
                        stopOnError = typeof action === 'string' || action.stopOnError,
                        waitForExit = typeof action === 'string' || action.waitForExit;

                    if (!waitForExit) {
                        exec(command, { env: process.env }).then(function () {
                            if (matchedHook.detailedLogging) {
                                console.info('Action %s of %s for %s succeeded!', index, matchedHook.name, request.identifier);
                            }
                        }, function (ex) {
                            console.warn('Action %s for %s failed: %s', index, request.identifier, ex);
                        });

                        return Promise.resolve().then(function () {
                            if (matchedHook.detailedLogging) {
                                console.info('Action %s of %s for %s started without waiting for it to finish.', index, matchedHook.name, request.identifier);
                            }
                        });
                    } else {
                        return Promise.resolve().then(function () {
                            if (matchedHook.detailedLogging) {
                                console.info('Action %s of %s for %s starting.', index, matchedHook.name, request.identifier);
                            }
                            return exec(command, { env: process.env });
                        }).then(function () {
                            if (matchedHook.detailedLogging) {
                                console.info('Action %s of %s for %s succeeded!', index, matchedHook.name, request.identifier);
                            }
                        }, function (ex) {
                            console.warn('Action %s for %s failed: %s', index, request.identifier, ex);

                            if (stopOnError) {
                                throw ex;
                            }
                        });
                    }
                });
            }, matchPromise);

            matchPromise.then(function () {
                if (hook.group && hook.id) {
                    processedHistory[hook.group] = processedHistory[hook.group] || [];
                    processedHistory[hook.group].push(eval(hook.id));
                }
                return Promise.resolve();
            });

            if (matchedHook.stopHere) {
                console.info('Stopped queueing hooks for %s', request.identifier);
                break;
            }
        }

        matchPromise.then(function () {
            if (matchActionCount === 0 && skippedHooks.length === 0) {
                console.info('No hooks or actions queued for %s!', request.identifier);

                if (config.logActionlessRequests) {
                    console.info('Logging request contents of %s:\r\nHeaders: %j\r\nBody: %j',
                                 request.identifier,
                                 request.headers,
                                 request.body);
                }
            } else if (matchActionCount !== 0) {
                console.info('Actions completed for %s!', request.identifier);
            }
        }, function () {
            console.error('Actions failed for %s.', request.identifier);
        });
    });
}

var hookListenServer = http.createServer(hookRequest);
hookListenServer.listen(config.port, function () {
    console.info('Now listening on port %s. %s hooks loaded.', config.port, hooks.length);
});

processedHistory =
    statFile(config.processedHistoryCache)
    .then(function () {
        return readFile(config.processedHistoryCache, 'utf8');
    })
    .then(
        function (historyJson) {
            try {
                processedHistory = JSON.parse(historyJson);
                return Promise.resolve(processedHistory);
            } catch (error) {
                return Promise.reject(error);
            }
        },
        function () {
            console.info('No processed hook history available.');
        }
    )
    .catch(function (error) {
        console.warn('Failed to load processed hook history.', error);
    });

function quit() {
    hookListenServer.close();

    writeFile(config.processedHistoryCache, JSON.stringify(processedHistory), 'utf8')
        .done(
            function () {
                console.info('Saved processed hook history. Quitting!');
                process.exit();
            },
            function (error) {
                console.error('Failed to save processed hook history.', error);
            }
        );
}

process.on('SIGTERM', function () {
    console.warn('SIGTERM received.');
    quit();
});

process.on('SIGINT', function () {
    console.warn('SIGINT received.');
    quit();
});
